import asyncio



class ClockEmulator:
    def __init__(self, frequency):
        self.frequency = frequency
        self.value = False

    async def generate_clock(self):
        while True:
            # Toggle the clock value between True and False
            self.value = not self.value
            await asyncio.sleep(1 / self.frequency)

    def get_value(self):
        return self.value

# Run the asynchronous program
async def main(clock_emulator):
    await asyncio.gather(clock_emulator.generate_clock())

if __name__ == "__main__":
    # Set the frequency to 10 MHz
    clock_emulator = ClockEmulator(frequency=10)

    try:
        # Create an event loop
        loop = asyncio.new_event_loop()

        # Start the asynchronous program using loop.run_until_complete
        loop.run_until_complete(main(clock_emulator))
    except KeyboardInterrupt:
        pass

    # Access the clock value from the same instance used in the asynchronous part
    value = clock_emulator.get_value()
    print(f"Final clock value: {value}")
