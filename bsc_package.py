
"""     FUNCTIONS AND CLASSES FOR MY BACHELOR'S THESIS


"""

import numpy as np
import scipy.signal as sig
from matplotlib import pyplot as plt 
import random
import librosa
import snntorch as snn
import torch
from snntorch import spikegen
from pylab import *
import pickle



def rate_coding(signal):

    # 1. Normalize the input signal
    signal = signal/np.max(signal)

    # 4. For each normalized value, val, in the signal
    # generate a random number, r from range (0,1). 
    # If val > r then store a spike at that time instant otherwise 0.

    spikeTrainOut = []

    for val in signal:

        spike = np.asarray([ int(1) if val > random.random() else int(-1) if -1*val > random.random()  else int(0) ])

        spikeTrainOut = np.hstack((spikeTrainOut, spike))

    return spikeTrainOut



def generate_sine_wave(duration, frequencies, amplitudes, sampling_rate):

    t = np.arange(0, duration, 1/sampling_rate)

    signal = np.sum([amplitude * np.sin(2 * np.pi * frequency * t) for frequency, amplitude in zip(frequencies, amplitudes)], axis=0)
    
    return t, signal

def activity_spike_converter(signal):

    currently_negative = True
    spk_trn = []

    for step in signal:
        if step <= 0:
            currently_negative = True
        
        if step > 0 and currently_negative:

            spk_trn.append(1)

            currently_negative = False

        else:

            spk_trn.append(0)
            
    return spk_trn


def spike_arr_gen(front_sig, delay_steps, period, num_steps, rand=False):

    # Function for creating spike arrays of desired period and delay between them!

    signal_left = []
    signal_right = []

    match front_sig.lower():

        case "left":

            if rand:
                for i in range(num_steps):
                    signal_left.append(1) if random.random() >= 0.5 else signal_left.append(0)
            

            else:

                signal_left = np.zeros(num_steps)

                for i in range(num_steps):
                    if i % period == 0:
                        signal_left[i] = 1

            if delay_steps == 0:
                signal_right = signal_left

                return signal_left, signal_right
            

            signal_right = np.zeros(delay_steps)

            signal_right = np.concatenate((signal_right, signal_left))

            signal_right = signal_right[:-delay_steps]



        case "right":

            if rand:
                for i in range(num_steps):
                    signal_right.append(1) if random.random() >= 0.5 else signal_right.append(0)


            else:

                signal_right = np.zeros(num_steps)

                for i in range(num_steps):
                    if i % period == 0:
                        signal_right[i] = 1

            if delay_steps == 0:
                signal_left = signal_right

                return signal_left, signal_right
            

            signal_left = np.zeros(delay_steps)

            signal_left = np.concatenate((signal_left, signal_right))

            signal_left = signal_left[:-delay_steps]



        case _: raise Exception("Invalid entry. Try 'left' or 'right' .")


    return signal_left, signal_right



# def activity_spike_converter2(signal):

    # Using sign changes instead, not necessarily useful
    
    currently_down = False
    spk_trn = []
    previous_step = 0

    for step in signal:
        if step < previous_step:
            currently_down = True
        
        if step > previous_step and currently_down:

            spk_trn.append(1)

            currently_down = False

        else:

            spk_trn.append(0)

        previous_step = step
            
    return spk_trn


# def leading_train(spk_trn_L, spk_trn_R):

    # Function for determining the leading signal, and to try and see how far behin dthe trailing signal is.

    determined_leading = 0
    lead_arr = []
    delay = 0
    LR_spk_intervals = []

    for step in range(len(spk_trn_L)):

        if delay == 0:
            lead_arr.append(determined_leading)
            determined_leading = 0

        if spk_trn_L[step] and not spk_trn_R[step] and determined_leading == 0:
            determined_leading = -1
        elif spk_trn_R[step] and not spk_trn_L[step] and determined_leading == 0:
            determined_leading = 1
        
        if determined_leading == -1:
            delay += 1

            if spk_trn_R[step]:
                LR_spk_intervals.append(delay)
                delay = 0

        elif determined_leading == 1:
            delay += 1

            if spk_trn_L[step]:
                LR_spk_intervals.append(delay)
                delay = 0


    return np.mean(LR_spk_intervals), np.mean(lead_arr)


# def generate_carfac_IR(signal_in = [], n_filters=100, fs = 44200.0, duration=1, init_freq=700):

    # Function for generating the impulse reponses for a CARFAC filter bank.

    fs = fs                                 # sample frequency
    dur = duration                          # simulation duration
    npoints = int(fs * dur)                 # stimulus length

    # create input tone used for finding variable b
    f0 = init_freq                                # tone frequency
    t1 = arange(npoints) / fs               # sample times
    gain = 0.1                              # input gain
    stimulus = gain * sin(2 * pi * f0 * t1) 

    nsec = n_filters                        # number of sections in the cochlea between
    xlow = 0.1                              # lowest frequency position along the cochlea and
    
    xhigh = 0.9                             # highest frequency position along the cochlea
    x = linspace(xhigh, xlow, nsec)         # position along the cochlea 1 = base, 0 = apex
    f = 165.4 * (10**(2.1 * x) - 1)         # Greenwood for humans
    a0 = cos(2 * pi * f / fs)               # a0 and c0 control the poles and zeros
    c0 = sin(2 * pi * f / fs)

    damping = 0.2                           # damping factor
    r = 1 - damping * 2 * pi * f / fs       # pole & zero radius (actual)
    r1 = 1 - damping * 2 * pi * f / fs      # pole & zero radius minimum (set point)
    h = c0                                  # p285 h = c0 puts the zeros 1/2 octave above poles
 
    g = (1 - 2 * a0 * r + r * r) / (1 - (2 * a0 - h * c0) * r + r * r)
                                            # p285 this gives 0dB DC gain for BM
    scale = 0.1                             # p297 NLF parameter
    offset = 0.04                           # p297 NLF parameter
    b = 1.0                                 # automatic gain loop feedback (1=no undamping).
    d_rz = 0.7 * (1 - r1)                   # p294 relative undamping

    f_hpf = 20                              # p313 20Hz corner for the BM HPF
    c_hpf = 1 / (1 + (2 * pi * f_hpf / fs)) # corresponding IIR coefficient 
    c = 20
    tau_res = 10e-3                         # p314 transmitter creation time constant
    a_res = 1 / (fs * tau_res)              # p314 corresponding IIR coefficient    
    tau_IHC = 80e-6                         # p314 ~8kHz LPF for IHC output
    c_IHC = 1 / (fs * tau_IHC)              # p314 corresponding IIR coefficient 
    
    
    # AGC loop parameters
    tau_AGC = .002 * 4**arange(4)       # p316

        # The remaining parameters are found in Google code, but not in the book.
        # The AGC filters are decimated, i.e., running at a lower sample rate, so the coefficients are calculated as:
    c_AGC = 8 * 2**arange(4) / (fs * tau_AGC)

        # spatial filtering
    shiftr_AGC = c_AGC * 0.65 * np.sqrt(2)**np.arange(4)
    spread_sq_AGC = c_AGC * (1.65**2 + 1) * 2**np.arange(4)
    c_L = (spread_sq_AGC + shiftr_AGC**2 - shiftr_AGC)/2
    c_R = (spread_sq_AGC + shiftr_AGC**2 + shiftr_AGC)/2
    c_I = 1 - c_L - c_R


    W = zeros(nsec)                     # BM filter internal state
    V = zeros(nsec)                     # BM filter internal state
    Vold = zeros(nsec)                  # BM filter internal state at t-1
    BM = zeros((nsec, npoints))         # BM displacement
    BM_hpf = zeros((nsec, npoints))     # BM displacement high-pass filtered at 20Hz
    tr_used = zeros(nsec)               # transmitter used 
    tr_reservoir = ones(nsec)           # transmitter available
    IHC = zeros((nsec, npoints))        # IHC output
    IHCa = zeros((nsec, npoints))       # IHC filter internal state
    BM[-1] = stimulus                   # put stimulus at BM[-1] to provide input to BM[0]
    BM[-1, -1] = 0                      # hack to make BM_hpf[nsec-1,0] work
    In8 = zeros(nsec)                   # Accumulator for ACG4
    In16 = zeros(nsec)                  # Accumulator for AGC3
    In32 = zeros(nsec)                  # Accumulator for AGC2
    In64 = zeros(nsec)                  # Accumulator for AGC1
    AGC = zeros((nsec,npoints))         # AGC filter internal state
    AGC0 = zeros(nsec)                  # AGC filter internal state
    AGC1 = zeros(nsec)                  # AGC filter internal state
    AGC2 = zeros(nsec)                  # AGC filter internal state
    AGC3 = zeros(nsec)                  # AGC filter internal state


    # RUNNING THE FIRST SIMULATION FOR A PURE TONE
    # THIS WILL FREEZE THE VARIABLE b, USED LATER
    for t in range(npoints):
        v_OHC = V - Vold
        Vold = V.copy()
        sqr = (v_OHC * scale + offset)**2
        NLF = 1 / (1 + (scale * v_OHC + offset)**2)
        r = r1 + d_rz * (1-b) * NLF
        g = (1 - 2 * a0 * r + r * r) / (1 - (2 * a0 - h * c0) * r + r * r)
        for s in range(nsec):
            Wnew = BM[s - 1, t] + r[s] * (a0[s] * W[s] - c0[s] * V[s])
            V[s] = r[s] * (a0[s] * V[s] + c0[s] * W[s])
            W[s] = Wnew
            BM[s, t] = g[s] * (BM[s-1, t] + h[s] * V[s])

        BM_hpf[:, t] = c_hpf * (BM_hpf[:, t-1] + BM[:, t] - BM[:, t-1])
        u = (BM_hpf[:, t] + 0.175).clip(0)  # p313
        v_mem = u**3 / (u**3 + u**2 + 0.1)  # p313, called 'g', but g is already used for the BM section gain
        tr_released = v_mem * tr_reservoir    # p313, called 'y', but renamed to avoid confusion
        tr_used = (1 - a_res) * tr_used + a_res * c * tr_released 
        tr_reservoir = 1 - tr_used            # p313, called 'v' in the book
        IHCa[:, t] = (1 - c_IHC) * IHCa[:, t-1] + c_IHC * tr_released
        IHC[:, t] = (1 - c_IHC) * IHC[:, t-1] + c_IHC * IHCa[:, t]

        In8 += IHC[:,t] / 8.0                                                       # accumulate input
        if t%64 == 0:                                                               # subsample AGC1 by factor 64
            AGC3 = (1 - c_AGC[3]) * AGC3 + c_AGC[3] * In64                          # LPF in time domain
            AGC3 = c_L[3] * roll(AGC3, 1) + c_I[3] * AGC3 + c_R[3] * roll(AGC3, -1) # LPF in spatial domain
            In64 *= 0                                                               # reset input accumulator for AGC3
        if t%32 == 0:                                                               # subsample AGC2 by factor 32
            AGC2 = (1 - c_AGC[2]) * AGC2 + c_AGC[2] * (In32 + 2 * AGC3)             # LPF in time domain
            AGC2 = c_L[2] * roll(AGC2, 1) + c_I[2] * AGC2 + c_R[2] * roll(AGC2, -1) # LPF in spatial domain
            In64 += In32                                                            # accumulate input for AGC3
            In32 *= 0                                                               # reset input accumulator for AGC2
        if t%16 == 0:                                                               # subsample ACG3 by factor 16
            AGC1 = (1 - c_AGC[1]) * AGC1 + c_AGC[1] * (In16 + 2 * AGC2)             # LPF in time domain
            AGC1 = c_L[1] * roll(AGC1, 1) + c_I[1] * AGC1 + c_R[1] * roll(AGC1, -1) # LPF in spatial domain
            In32 += In16                                                            # accumulate input for AGC2
            In16 *= 0                                                               # reset input accumulator for AGC1
        if t%8 == 0:                                                                # subsample AGC0 by factor 8
            AGC0 = (1 - c_AGC[0]) * AGC0 + c_AGC[0] * (In8 + 2 * AGC1)              # LPF in time domain
            AGC0 = c_L[0] * roll(AGC0, 1) + c_I[0] * AGC0 + c_R[0] * roll(AGC0, -1) # LPF in spatial domain
            AGC[:,t] = AGC0                                                         # store AGC output for plotting
            In16 += In8                                                             # accumulate input for AGC1
            In8 *= 0                                                                # reset input accumulator for AGC0
            b = AGC0                                                                # b of OHC is equal to AGC0
            r = r1 + d_rz * (1 - b) * NLF                                           # feedback to BM
            g = (1 - 2 * a0 * r + r * r) / (1 - (2 * a0 - h * c0) * r + r * r)      # gain for BM
        else:
            AGC[:,t] = AGC[:, t - 1]


    # Now measure the frequency response of the cochlear filters
    # create a log-sine-sweep
    f0 = 10                                          # sweep start frequency
    f1 = fs / 2                                      # sweep end frequency
    if len(signal_in) == 0:
        stimulus = sig.chirp(t1, f0, t1[-1], f1, method='logarithmic', phi=-90)
    else:
        stimulus = signal_in
    # wl = 1000                                        # window length
    # stimulus[:wl] *= sin(linspace(0, .5 * pi, wl))   # ramp up start
    # stimulus[-wl:] *= cos(linspace(0, .5 * pi, wl))  # ramp down end


    # reset all these states
    W0 = zeros(nsec)                 # BM filter internal state
    W1 = zeros(nsec)                 # BM filter internal state
    W1old = zeros(nsec)              # BM filter internal state at t-1
    BM = zeros((nsec, npoints))      # BM displacement
    BM_hpf = zeros((nsec, npoints))  # BM displacement high-pass filtered at 20Hz
    tr_used = zeros(nsec)             # transmitter used 
    tr_reservoir = ones(nsec)         # transmitter available
    IHC = zeros((nsec, npoints))     # IHC output
    IHCa = zeros((nsec, npoints))    # IHC filter internal state


    # now play sweep through cochlea while keeping b fixed to measure the frequency response
    BM[-1] = stimulus                # put stimulus at BM[-1] to provide input to BM[0]
    BM[-1, -1] = 0                   # hack to make BM_hpf[nsec-1, 0] work
    for t in range(npoints):
        v_OHC = V - Vold
        Vold = V.copy()
        sqr = (v_OHC * scale + offset)**2
        NLF = 1 / (1 + (scale * v_OHC + offset)**2)
        r = r1 + d_rz * (1-b) * NLF
        g = (1 - 2 * a0 * r + r * r) / (1 - (2 * a0 - h * c0) * r + r * r)
        for s in range(nsec):
            Wnew = BM[s - 1, t] + r[s] * (a0[s] * W[s] - c0[s] * V[s])
            V[s] = r[s] * (a0[s] * V[s] + c0[s] * W[s])
            W[s] = Wnew
            BM[s, t] = g[s] * (BM[s-1, t] + h[s] * V[s])

        BM_hpf[:, t] = c_hpf * (BM_hpf[:, t-1] + BM[:, t] - BM[:, t-1])
        u = (BM_hpf[:, t] + 0.175).clip(0)  # p313
        v_mem = u**3 / (u**3 + u**2 + 0.1)  # p313, called 'g', but g is already used for the BM section gain
        tr_released = v_mem * tr_reservoir    # p313, called 'y', but renamed to avoid confusion
        tr_used = (1 - a_res) * tr_used + a_res * c * tr_released 
        tr_reservoir = 1 - tr_used            # p313, called 'v' in the book
        IHCa[:, t] = (1 - c_IHC) * IHCa[:, t-1] + c_IHC * tr_released
        IHC[:, t] = (1 - c_IHC) * IHC[:, t-1] + c_IHC * IHCa[:, t]

    # use the FFT of the stimulus and output directly to calculate the transfer function
    FL = ceil(log2(npoints))            # define FFT length
    output = BM                         # use this signal as the output signal
    myFFT = fft(zeros((nsec, int(2**FL))))
    for s in range(nsec):
        myFFT[s] = fft(output[s], int(2**FL)) / fft(stimulus, int(2**FL))

    # calculate the BM impulse response
    IR = real(ifft(myFFT))
    IR[:, 0] = zeros(nsec)              # remove artefact

    print(f"Generated an array of IRs for {n_filters}, each spanning a range of {(f1-f0)/n_filters} Hz.")
    # return BM
    return IR




## ------------------ CLASSES ------------------- ##


class iF():
    # This class defines a regular integrate and Fire neuron. It is a non-leaky version of the LiF, described below

    def __init__(self, params) -> None:
        # Initializes the IF neuron with relevant parameters
        self.w = params[0]
        self.beta = params[1]
        self.threshold = params[2]
        self.mem = 0
        self.spk = 0

    def run_step(self, input_current):

        if self.mem > self.threshold:

            self.spk = 1 # if membrane exceeds threshold, spk=1, else,
            self.mem = 0

        else: self.spk = 0
        
        if input_current != 0:

            self.mem = self.beta * self.mem + self.w*input_current

        else: self.mem = self.mem



class LiF():

    # This class defines a Leaky integrate and Fire neuron following the model described in

    # https://snntorch.readthedocs.io/en/latest/tutorials/tutorial_3.html 


    def __init__(self, params) -> None:

        # Initializes the LiF neuron with relevant parameters

        self.w = params[0]
        self.beta = params[1]
        self.threshold = params[2]
        self.refract = params[3]
    

        
        self.downtime = 0

        self.mem = 0.0
        self.spk = 0


    def run_step(self, input_current):

        # This function takes an input current and calculates the output based on the membrane potential.
        # The membrane potential is also calculated in the process.
        # This function is supposed to be run for each time step in a given signal.

        if self.mem > self.threshold:
            self.spk = 1 # if membrane exceeds threshold, spk=1, else, 0
            self.downtime = self.refract
            self.mem = 0 #self.spk * self.threshold

        else: self.spk = 0


        if self.downtime == 0:
            self.mem = self.beta * self.mem + self.w*input_current

        else: self.downtime -= 1

        # return spk



class TDE():

    # This class defines a Time Difference Encoder element.
    # The element primarily contains a decaying exponential function and a single LiF element

    def __init__(self, decay_params=[1, 0.5, 20], LIF_params=[0.4, 0.819, 1, 0], leaky=True) -> None:

        """
        Parameters:
        decay_params (list): List consisting of decay parameters - initial value , decay factor , num of samples
        LIF_params (list): List containing parameters for the LIF - weighted input parameter , decay value , threshold value
        """


        # Initializes the exponential function to grab gain values from

        self.exp = np.append(self.exponential_decay(decay_params[0], decay_params[1], decay_params[2]), 0)

        self.gain = 0           # The current gain of the TDE
        self.gain_index = np.size(self.exp)     # The index of the current gain, to be used with the exponential decay function

        if leaky:
            self.neuron = LiF(LIF_params)

        else: self.neuron = iF(LIF_params)

        self.send_current = True

        self.output = 0

        self.volt_out = 0


    def exponential_decay(self, a, b, N):

        # a, b: exponential decay parameter
        # N: number of samples 

        return a * (1-b) ** np.arange(N)

    def calculate(self, fac, trig):


        # If a facilitatory spike is recieved, the gain is set to be the first value of the calculated exponential decay function
        if fac == 1:
            self.send_current = False
            self.gain_index = 0
            self.gain = self.exp[self.gain_index]


        # If a trigger spike is recived current is only sent to the LiF if gain != 0
        # The necessity of this is that the gain needs to be sent to the LiF continuously after a trigger is received
        if self.gain != 0 and trig == 1:
            self.send_current = True

        if self.send_current:
            self.volt_out = self.gain
            
        else:
            self.volt_out = 0.0
        
        self.neuron.run_step(self.volt_out)
        spk = self.neuron.spk

        # This part increments the index of the gain value to simulate a continuous decay
        if self.gain_index < np.size(self.exp):
            self.gain = self.exp[self.gain_index]
            self.gain_index += 1
        
        self.output = spk

    

    # Return functions

    def return_lif_mem(self):
        return self.neuron.mem
    
    def return_lif_gain(self):
        return self.gain
    



class SHaF:

    # A class for the Spike Hold and Fire block, described in Jimenez-Fernandez2011
    # The SHaF block does spike train multiplication 

    def __init__(self):
        self.U = 0
        self.Y = 0
        self.state = ""
        self.current_out = 0

    def input(self, u = 0, y = 0):
        self.U = u
        self.Y = y

    def output(self):
        return self.current_out


    def calculate(self):

        match self.state:
            case "Up":
                if self.U == 1:
                    self.state = "Up"
                    self.current_out = 1
          

                elif self.Y == 1:
                    self.state = ""
                    self.current_out = 0
   
                    
                elif self.U == -1:
                    self.state = ""
                    self.current_out = 0
         

                elif self.Y == -1:
                    self.state = "Up"
                    self.current_out = 1
             
            case "Un":

                if self.U == 1:
                    self.state = ""
                    self.current_out = 0
           

                elif self.Y == 1:
                    self.state = "Un"
                    self.current_out = -1
             

                elif self.U == -1:
                    self.state = "Un"
                    self.current_out = -1
             
                
                elif self.Y == -1:
                    self.state = ""
                    self.current_out = 0
              
        
            case "Yp":

                if self.U == 1:
                    self.state = ""
                    self.current_out = 0
                    

                elif self.Y == 1:
                    self.state = "Yp"
                    self.current_out = 1

                elif self.U == -1:
                    self.state = "Yp"
                    self.current_out = 1
                
                elif self.Y == -1:
                    self.state = ""
                    self.current_out = 0
        
            case "Yn":

                if self.U == 1:
                    self.state = "Yn"
                    self.current_out = -1

                elif self.Y == 1:
                    self.state = ""
                    self.current_out = 0
                    
                elif self.U == -1:
                    self.state = ""
                    self.current_out = 0
                    

                elif self.Y == -1:
                    self.state = "Yn"
                    self.current_out = -1
                
            case "":
                if self.U == 1:
                    self.state = "Up"
                    self.current_out = 0
                
                elif self.U == -1:
                    self.state = "Un"
                    self.current_out = 0

                elif self.Y == 1:
                    self.state = "Yp"
                    self.current_out = 0

                elif self.Y == -1:
                    self.state = "Yn"
                    self.current_out = 0

            

### UNDER DEVELOPEMENT ###
class SIaG:

    # Class for the Spike Integrate and Generate block described in Jimenez-Fernandez2011

    def __init__(self, spike_threshold:int ):
        self.spikes_Count = 0
        self.spike_threshold = spike_threshold
    
    def process_spike(self, spike):

        self.spikes_Count += spike

        if self.spikes_Count > 0 and self.spikes_Count >= self.spike_threshold:
            
            self.spikes_Count = 0

            return 1
        
        elif self.spikes_Count < 0 and self.spikes_Count <= -1 * self.spike_threshold:
            
            self.spikes_Count = 0

            return -1
        
        else: return 0


    def calc_train(self, spike_train):

        output_train = []

        for s in spike_train:
            res = self.process_spike(s)

            output_train = np.append(output_train, res)

        return output_train



### UNDER DEVELOPEMENT ### 
class SLPF2():

    # Class for the Spiking Low-Pass Filter block described in Jimenez-Fernandez2011

    # This version is made such that "process_spike() is step by step"

    def __init__(self, thres) -> None:

        self.HaF_internal = SHaF()
        self.HaF_activity = SHaF()

        self.IaG_internal = SIaG(spike_threshold=thres)

    
    def process_spike(self, spk):

        # Internals

        
        self.HaF_internal.U = spk                   # Input spike is defined
        self.HaF_internal.calculate()               # The state of the block and the subsequent output is calculated

        spike_out = self.IaG_internal.process_spike(self.HaF_internal.current_out)    # The SI&G step is handled

        # spike_out = self.HaF_internal.current_out

        
        self.HaF_internal.Y = spike_out                                 # The calculated output is given to the SHaF as feedback input

        
        self.HaF_activity.U = spk
        self.HaF_activity.Y = spike_out      

        self.HaF_activity.calculate()

        activity_out = self.HaF_activity.current_out


        return spike_out, activity_out




class TimeToRate():

    # This class employs a series of TDE elements and LIF elements. The resulting object will mimic one layer of the Time to Rate network described by Schoepe2023.

    def __init__(self, tde_exp_init_val=3, 
                        tde_exp_num_steps=40, 
                        tde_exp_decay_fact=[0.5, 0.25, 0.125, 0.0625],                  
                        LIF_params=[0.4, 0.819, 1, 0],
                        main_LIF_params = [0.4, 0.819, 1, 0],
                        inhib_v=0.5, 
                        excite_v=0.5, 
                        lif_inhib_v=0.5) -> None:
        
        self.lif_L = LiF(main_LIF_params)
        self.lif_R = LiF(main_LIF_params)

        ## Left TDE population
        self.tde0_L = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[0],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)
        
        self.tde1_L = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[1],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)
        
        self.tde2_L = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[2],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)
        
        self.tde3_L = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[3],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)


        ## Right TDE population
        self.tde0_R = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[0],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)
        
        self.tde1_R = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[1],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)
        
        self.tde2_R = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[2],
                           tde_exp_num_steps],
                           LIF_params=LIF_params)
        
        self.tde3_R = TDE([tde_exp_init_val, 
                           tde_exp_decay_fact[3],
                           tde_exp_num_steps], 
                           LIF_params=LIF_params)
        
        ## Excitatory and inhibitory voltages
        self.inhib_v = inhib_v
        self.excite_v = excite_v
        self.lif_inhib_v = lif_inhib_v

        ## The output of the two LIFs
        self.left_out = 0
        self.right_out = 0

    
    def calculate_step(self, input_L, input_R):

        # Run left TDE population

        self.tde0_L.calculate(fac=input_L, trig=input_R)
        self.tde1_L.calculate(fac=input_L, trig=input_R)
        self.tde2_L.calculate(fac=input_L, trig=input_R)
        self.tde3_L.calculate(fac=input_L, trig=input_R)

            # If any of the first set of TDEs spikes, inhibit the left LIF by a predefined voltage
        left_pop_inhib_voltage = self.inhib_v if (self.tde0_L.output or 
                                                   self.tde1_L.output or 
                                                   self.tde2_L.output) else 0 
        
        # left_pop_inhib_voltage = self.inhib_v * np.sum([self.tde0_L.output, self.tde1_L.output, self.tde2_L.output]) 

        

        # Run right TDE population

        self.tde0_R.calculate(fac=input_R, trig=input_L)
        self.tde1_R.calculate(fac=input_R, trig=input_L)
        self.tde2_R.calculate(fac=input_R, trig=input_L)
        self.tde3_R.calculate(fac=input_R, trig=input_L)

            # If any of the first set of TDEs spikes, inhibit the right LIF by a predefined voltage
        right_pop_inhib_voltage = self.inhib_v if (self.tde0_R.output or 
                                                    self.tde1_R.output or 
                                                    self.tde2_R.output) else 0
        # right_pop_inhib_voltage = self.inhib_v * np.sum([self.tde0_R.output, self.tde1_R.output, self.tde2_R.output]) 
        
        
        ## If either fourth TDE spikes, excite the right LIF by a voltage
        excitatory_voltage = self.excite_v if (self.tde3_R.output or self.tde3_L.output) else 0


            ## Here the voltages to send to each LIF is found. Note that the voltage for the left LIF is defined by the left fourth TDE and the RIGHT population of TDEs. Vice versa for the right LIF. 
        total_left_lif_voltage_input = excitatory_voltage - right_pop_inhib_voltage
        total_right_lif_voltage_input = excitatory_voltage - left_pop_inhib_voltage


        # Run LIF step


            ## The two LIFs inhibit each other. To ensure that the spikeing behaviour of each LIF is calculated at the same time, a boolean for each is declared.

        left_lif_will_spike = True if self.lif_L.mem + total_left_lif_voltage_input > self.lif_L.threshold else False
        right_lif_will_spike = True if self.lif_R.mem + total_right_lif_voltage_input > self.lif_R.threshold else False

            ## If a left spike was coming, inhibit right LIF 
        if left_lif_will_spike:
            self.lif_R.mem -= self.lif_inhib_v
        
            ## If a right spike was coming, inhibit left LIF
        if right_lif_will_spike:
            self.lif_L.mem -= self.lif_inhib_v

            ## Calculate the LIF spikes
        self.lif_L.run_step(total_left_lif_voltage_input)
        self.lif_R.run_step(total_right_lif_voltage_input)

            ## Set network layer outputs
        self.left_out = self.lif_L.spk
        self.right_out = self.lif_R.spk
